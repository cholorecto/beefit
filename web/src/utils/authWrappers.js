import { UserAuthWrapper } from 'redux-auth-wrapper'
import _ from 'lodash'
import { replace } from 'react-router-redux'
import { toast } from 'react-toastify'

export const UserIsAuthenticated = UserAuthWrapper({
  authSelector: state => state.auth,
  wrapperDisplayName: 'UserIsAuthenticated',
  predicate: auth => {
    return auth && auth.get('user')
  },
  redirectAction: (newLoc) => (dispatch) => {
    dispatch(replace(newLoc))
    dispatch(toast.error('Access denied. Login first.'))
  }
})

export const UserIsAlreadyLoggedIn = UserAuthWrapper({
  authSelector: state => state.auth,
  wrapperDisplayName: 'UserIsAlreadyLoggedIn',
  predicate: auth => {
    return (JSON.parse(localStorage.getItem('reduxPersist:auth')) && !JSON.parse(localStorage.getItem('reduxPersist:auth')).user) || !JSON.parse(localStorage.getItem('reduxPersist:auth'))
  },
  failureRedirectPath: '/dashboard',
  allowRedirectBack: false,
  redirectAction: (newLoc) => (dispatch) => {
    dispatch(replace(newLoc))
    dispatch(toast.warn('You are already logged in. If you wish to switch account, logout first.'))
  }
})

