import React, { Component } from 'react'
import {
  Field,
  Control,
  Label,
  Select,
  Help,
} from 'react-bulma-components/lib/components/form';

class SelectFieldGroup extends Component {
  render () {
    const { name, value, error, onChange, options, placeholder } = this.props
    return (
      <Field>
        <Control>
          <Select name={name} onChange={onChange} value={value}>
            <option value={''} key={value} disabled>{placeholder}</option>
            {options && options.map(item => {
              return <option value={item} key={item}>{item}</option>
            })}
          </Select>
          <Help color={error && 'danger'}>{error}</Help>
        </Control>
      </Field>
    )
  }
}

SelectFieldGroup.propTypes = {

}

export default SelectFieldGroup
