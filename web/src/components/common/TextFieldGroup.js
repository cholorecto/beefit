import React, { Component } from 'react'
import {
  Field,
  Control,
  Label,
  Input,
  Textarea,
  Select,
  Checkbox,
  Radio,
  Help,
} from 'react-bulma-components/lib/components/form';

class TextFieldGroup extends Component {
  render () {
    const { type, name, value, placeholder, error, help, onChange, disabled, withLabel } = this.props
    return (
      <Field>
        {withLabel && <Label>{withLabel}</Label>}
        <Control>
          <Input color={error && 'danger'} onChange={onChange} name={name} type={type} placeholder={placeholder} value={value} disabled={disabled} />
          <Help color={error && 'danger'}>{error}</Help>
        </Control>
      </Field>
    )
  }
}

TextFieldGroup.propTypes = {

}

export default TextFieldGroup
