import React, { Component } from 'react'
import { Link } from 'react-router'

class NavigationBar extends Component {

  render () {
    return (
      <div>
        <nav className='navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light' id='ftco-navbar'>
          <div className='container'>
            <a className='navbar-brand' href='index.html'>BeeFit<small>Gym Plus</small></a>
            <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#ftco-nav' aria-controls='ftco-nav' aria-expanded='false' aria-label='Toggle navigation'>
              <span className='oi oi-menu'></span> Menu
            </button>
            <div className='collapse navbar-collapse' id='ftco-nav'>
              <ul className='navbar-nav ml-auto'>
                <li className='nav-item'><a href='#process' className='nav-link'>Process</a></li>
                <li className='nav-item'><a href='#programs' className='nav-link'>Programs</a></li>
                <li className='nav-item'><a href='#coaches' className='nav-link'>Coaches</a></li>
                <li className='nav-item'><a href='#packages' className='nav-link'>Packages</a></li>
                <li className='nav-item'><a href='#footer' className='nav-link'>Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

NavigationBar.propTypes = {

}

export default NavigationBar
