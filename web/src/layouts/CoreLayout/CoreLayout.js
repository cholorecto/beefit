import React, { Component } from 'react'
import 'react-bulma-components/src/index.sass'
import '../../styles/core.scss'

import Navbar from '../../components/common/NavigationBar'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

class CoreLayout extends Component {

  render () {
    return (
      <div className='core'>
        <Navbar {...this.props} />
        {this.props.children}
      </div>
    )
  }
}

CoreLayout.propTypes = {

}

export default CoreLayout
