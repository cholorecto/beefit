import { connect } from 'react-redux'

import CoreLayout from '../CoreLayout'
import { logout } from 'store/modules/auth'

const mapActionCreators = {
  logout
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, mapActionCreators)(CoreLayout)

