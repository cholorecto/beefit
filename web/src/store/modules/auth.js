import Immutable from 'immutable'
import { CALL_API } from 'redux-api-middleware'
import { REHYDRATE } from 'redux-persist/constants'
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export const LOGIN = 'auth/LOGIN'
export const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS'
export const LOGIN_FAIL = 'auth/LOGIN_FAIL'

export const LOAD = 'auth/LOAD'
export const LOAD_SUCCESS = 'auth/LOAD_SUCCESS'
export const LOAD_FAIL = 'auth/LOAD_FAIL'

export const LOGOUT = 'auth/LOGOUT'
export const LOGOUT_SUCCESS = 'auth/LOGOUT_SUCCESS'

export const REGISTER = 'auth/REGISTER'
export const REGISTER_SUCCESS = 'auth/REGISTER_SUCCESS'
export const REGISTER_FAIL = 'auth/REGISTER_FAIL'

export const RESET_PASSWORD = 'auth/RESET_PASSWORD'
export const RESET_PASSWORD_SUCCESS = 'auth/RESET_PASSWORD_SUCCESS'
export const RESET_PASSWORD_FAIL = 'auth/RESET_PASSWORD_FAIL'

export const UPDATE_ACCOUNT = 'auth/UPDATE_ACCOUNT'
export const UPDATE_ACCOUNT_SUCCESS = 'auth/UPDATE_ACCOUNT_SUCCESS'
export const UPDATE_ACCOUNT_FAIL = 'auth/UPDATE_ACCOUNT_FAIL'

// ------------------------------------
// Actions
// ------------------------------------

export function login (data) {
  return {
    [CALL_API]: {
      endpoint: '/api/v1/accounts/login',
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
      types: [
        LOGIN,
        {
          type: LOGIN_SUCCESS,
          meta: {
            done: true,
            transition: {
              success: (prevState, nextState) => {
                const { query, pathname } = prevState.router.locationBeforeTransitions

                const redirectTo = pathname === '/login' ? '/dashboard' : '/dashboard'

                window.location = query.redirect || redirectTo
              }
            }
          }
        },
        LOGIN_FAIL]
    }
  }
}

export function register (data) {
  return {
    [CALL_API]: {
      endpoint: '/api/v1/accounts/signup',
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
      types: [
        REGISTER,
        {
          type: REGISTER_SUCCESS,
          meta: {
            done: true,
            transition: {
              success: (prevState, nextState) => {
                const redirectTo = '/login'
                window.location = redirectTo
              }
            }
          }
        },
        REGISTER_FAIL]
    }
  }
}

export function updateAccount (data, id) {
  return (dispatch, getState) => {
    dispatch(showLoading())
    let endpoint = `/api/v1/account/${id}`
    const { accessToken } = getState().auth.toJS()
    return dispatch({
      [CALL_API]: {
        endpoint,
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        },
        types: [UPDATE_ACCOUNT, UPDATE_ACCOUNT_SUCCESS, UPDATE_ACCOUNT_FAIL]
      }
    }).then(() => { dispatch(hideLoading()) })
  }
}

export function resetPassword (email) {
  return {
    [CALL_API]: {
      endpoint: `/api/v1/accounts/password/reset?email=${email}`,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      types: [
        RESET_PASSWORD,
        RESET_PASSWORD_SUCCESS,
        RESET_PASSWORD_FAIL]
    }
  }
}

export function resetPasswordConfirm (data) {
  return {
    [CALL_API]: {
      endpoint: `/api/v1/accounts/password/confirm?email=${data.email}&reset_password_hash=${data.reset_password_hash}`,
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
      types: [
        RESET_PASSWORD,
        {
          type: RESET_PASSWORD_SUCCESS,
          meta: {
            done: true,
            transition: {
              success: (prevState, nextState) => {
                const redirectTo = '/login'
                window.location = redirectTo
              }
            }
          }
        },
        RESET_PASSWORD_FAIL]
    }
  }
}

export function load (token) {
  return {
    [CALL_API]: {
      endpoint: '/api/v1/accounts/me',
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      },
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL]
    }
  }
}

export function logout () {
  return async (dispatch) => {
    await dispatch({
      type: LOGOUT
    })
    await dispatch({
      type: LOGOUT_SUCCESS,
      meta: {
        done: true,
        transition: {
          success: async (prevState, nextState) => {
            await localStorage.removeItem('reduxPersist:auth')
            window.location = '/'
          }
        }
      }
    })
  }
}

export const actions = {
  login,
  load,
  logout,
  register,
  resetPassword,
  resetPasswordConfirm,
  updateAccount
}

// ------------------------------------
// Action Handlers
// ------------------------------------

const actionHandlers = {}

// ------------------------------------
// Rehydrate store action handler
// ------------------------------------

actionHandlers[REHYDRATE] = (state, action) => {
  const incoming = action.payload.auth

  return state.merge({
    accessToken: incoming && incoming.accessToken
  })
}

actionHandlers[LOGIN] = state => state.merge({
  creatingUserSuccess: false,
  loggingIn: true,
  loginSuccess: false,
  loginError: null
})

actionHandlers[LOGIN_SUCCESS] = (state, action) => state.merge({
  loggingIn: false,
  loginSuccess: true,
  user: action.payload.data.user,
  accessToken: action.payload.data.accessToken,
  loginError: null
})

actionHandlers[LOGIN_FAIL] = (state, action) => state.merge({
  loggingIn: false,
  loginSuccess: false,
  loginError: action.payload.response.error
})

actionHandlers[LOAD] = (state, action) => state.merge({
  loading: false,
  loadError: null,
  loaded: true
})

actionHandlers[LOAD_SUCCESS] = (state, action) => {
  return state.merge({
    loading: false,
    loadError: null,
    loaded: true,
    user: action.payload
  })
}

actionHandlers[LOAD_FAIL] = (state, action) => state.merge({
  loading: false,
  loadError: action.payload.response.error,
  loaded: false,
  accessToken: null, // clear token on failure
  user: null
})

actionHandlers[REGISTER] = state => state.merge({
  creatingUser: true,
  creatingUserSuccess: false,
  creatingUserError: null
})

actionHandlers[REGISTER_SUCCESS] = (state, action) => state.merge({
  creatingUser: false,
  creatingUserSuccess: true,
  creatingUserError: null,
  createdUser: action.payload.data
})

actionHandlers[REGISTER_FAIL] = (state, action) => state.merge({
  creatingUser: false,
  creatingUserSuccess: false,
  creatingUserError: action.payload.response.error
})

actionHandlers[RESET_PASSWORD] = state => state.merge({
  resettingPassword: true,
  resettingPasswordSuccess: false,
  resettingPasswordError: null
})

actionHandlers[RESET_PASSWORD_SUCCESS] = (state, action) => state.merge({
  resettingPassword: false,
  resettingPasswordSuccess: true,
  resettingPasswordError: null
})

actionHandlers[RESET_PASSWORD_FAIL] = (state, action) => state.merge({
  resettingPassword: false,
  resettingPasswordSuccess: false,
  resettingPasswordError: action.payload.response.error
})

actionHandlers[UPDATE_ACCOUNT] = state => state.merge({
  updatingUser: true,
  updatingUserSuccess: false,
  updatingUserError: null
})

actionHandlers[UPDATE_ACCOUNT_SUCCESS] = (state, action) => state.merge({
  updatingUser: false,
  updatingUserSuccess: true,
  updatingUserError: null,
  user: action.payload
})

actionHandlers[UPDATE_ACCOUNT_FAIL] = (state, action) => state.merge({
  updatingUser: false,
  updatingUserSuccess: false,
  updatingUserError: action.payload.response.error
})

// ------------------------------------
// Reducer
// ------------------------------------

const initialState = Immutable.fromJS({
  user: null,
  accessToken: null,
  // login
  loggingIn: null,
  loginError: null,
  loginSuccess: false,
  // load
  loadError: null,
  // register
  creatingUser: false,
  creatingUserSuccess: false,
  creatingUserError: null,
  createdUser: null,
  // reset
  resettingPassword: false,
  resettingPasswordSuccess: false,
  resettingPasswordError: null,
  // update
  updatingUser: false,
  updatingUserSuccess: false,
  updatingUserError: null
})

export default function reducer (state = initialState, action) {
  const handler = actionHandlers[action.type]

  return handler ? handler(state, action) : state
}
