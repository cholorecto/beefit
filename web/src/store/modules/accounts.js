import Immutable from 'immutable'
import { CALL_API } from 'redux-api-middleware'
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export const GET_ACCOUNTS = 'api/GET_ACCOUNTS'
export const GET_ACCOUNTS_SUCCESS = 'api/GET_ACCOUNTS_SUCCESS'
export const GET_ACCOUNTS_FAIL = 'api/GET_ACCOUNTS_FAIL'

export const WRITE_ACCOUNT = 'api/WRITE_ACCOUNT'
export const WRITE_ACCOUNT_SUCCESS = 'api/WRITE_ACCOUNT_SUCCESS'
export const WRITE_ACCOUNT_FAIL = 'api/WRITE_ACCOUNT_FAIL'

// ------------------------------------
// Actions
// ------------------------------------

export function getAccounts (id) {
  return (dispatch, getState) => {
    dispatch(showLoading())
    let endpoint = '/api/v1/accounts'
    const { accessToken } = getState().auth.toJS()
    return dispatch({
      [CALL_API]: {
        endpoint,
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${accessToken}`
        },
        types: [GET_ACCOUNTS, GET_ACCOUNTS_SUCCESS, GET_ACCOUNTS_FAIL]
      }
    }).then(() => { dispatch(hideLoading()) })
  }
}

export function updateAccountStatus (data, id) {
  return (dispatch, getState) => {
    dispatch(showLoading())
    let endpoint = `/api/v1/accounts/overall/status/${id}`
    const { accessToken } = getState().auth.toJS()
    return dispatch({
      [CALL_API]: {
        endpoint,
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        },
        types: [WRITE_ACCOUNT, WRITE_ACCOUNT_SUCCESS, WRITE_ACCOUNT_FAIL]
      }
    }).then(() => { dispatch(hideLoading()) })
  }
}

export function updateAccountAlumniStatus (data, id) {
  return (dispatch, getState) => {
    dispatch(showLoading())
    let endpoint = `/api/v1/accounts/alumni/status/${id}`
    const { accessToken } = getState().auth.toJS()
    return dispatch({
      [CALL_API]: {
        endpoint,
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        },
        types: [WRITE_ACCOUNT, WRITE_ACCOUNT_SUCCESS, WRITE_ACCOUNT_FAIL]
      }
    }).then(() => { dispatch(hideLoading()) })
  }
}

export const actions = {
  getAccounts,
  updateAccountStatus,
  updateAccountAlumniStatus
}

// ------------------------------------
// Action Handlers
// ------------------------------------

const actionHandlers = {}

// ------------------------------------
// Rehydrate store action handler
// ------------------------------------

actionHandlers[ GET_ACCOUNTS ] = state => {
  return state.merge({
    fetchingAccounts: true,
    fetchingAccountsSuccess: false,
    fetchingAccountsError: null
  })
}

actionHandlers[ GET_ACCOUNTS_SUCCESS ] = (state, action) => {
  return state.merge({
    fetchingAccounts: false,
    fetchingAccountsSuccess: true,
    fetchingAccountsError: null,
    accountsData: action.payload.data.accountsData
  })
}

actionHandlers[ GET_ACCOUNTS_FAIL ] = (state, action) => {
  return state.merge({
    fetchingAccounts: false,
    fetchingAccountsSuccess: false,
    fetchingAccountsError: action.payload.response.error
  })
}

actionHandlers[ WRITE_ACCOUNT ] = state => {
  return state.merge({
    writingAccount: true,
    writingAccountSuccess: false,
    writingAccountError: null
  })
}

actionHandlers[ WRITE_ACCOUNT_SUCCESS ] = (state, action) => {
  return state.merge({
    writingAccount: false,
    writingAccountSuccess: true,
    writingAccountError: null,
    accountsData: action.payload.data.accountsData
  })
}

actionHandlers[ WRITE_ACCOUNT_FAIL ] = (state, action) => {
  return state.merge({
    writingAccount: false,
    writingAccountSuccess: false,
    writingAccountError: action.payload.response.error
  })
}

// ------------------------------------
// Reducer
// ------------------------------------

const initialState = Immutable.fromJS({
  fetchingAccounts: false,
  fetchingAccountsSuccess: false,
  fetchingAccountsError: null,
  accountsData: null
})

export default function reducer (state = initialState, action) {
  const handler = actionHandlers[ action.type ]

  return handler ? handler(state, action) : state
}
