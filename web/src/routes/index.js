// We only need to import the modules necessary for initial render
import CoreLayoutContainer from '../layouts/CoreLayout/containers/CoreLayoutContainer'
import Home from './Home'
// import Ws from '@adonisjs/websocket-client'

// const ws = Ws('ws://localhost:3333')
// const mall = ws.subscribe('mall:items')

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
  path: '/',
  onEnter: async (nextState, replace, cb) => {
    // const auth = JSON.parse(localStorage.getItem('reduxPersist:auth'))
    // if (auth && auth.accessToken) {
    //   const authActions = require('store/modules/auth').actions
    //   await store.dispatch(authActions.load(auth.accessToken))
    // }
    cb()
  },
  component: CoreLayoutContainer,
  indexRoute: Home,
  childRoutes: [
  ]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
