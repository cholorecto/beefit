import React, { Component } from 'react'

import Header from './assets/Header'
import Body from './assets/Body'

class Home extends Component {
  render () {
    return (
      <div>
        <Header {...this.props} />
        <Body />
      </div>
    )
  }
}

Home.propTypes = {

}

export default Home
