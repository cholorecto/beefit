import React, { Component } from 'react'

class Header extends Component {

  render () {
    return (
      <div>
        <section className='home-slider owl-carousel'>
          <div className='slider-item' style={{backgroundImage: 'url("../assets/images/index.jpg")'}} data-stellar-background-ratio='0.5'>
            <div className='overlay'></div>
            <div className='container'>
              <div className='row slider-text justify-content-center align-items-center'>
                <div className='col-md-8 col-sm-12 text-center ftco-animate'>
                  <h1 style={{fontSize: '40px'}} className='mb-3 mt-5 bread'>Do something <span className='text-primary'>today</span> that your <span className='text-primary'>future self</span> will thank you for</h1>
                  <p><span className='mr-2'>BeeFit Gym Plus</span></p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

Header.propTypes = {

}

export default Header
