import React, { Component } from 'react'

class Body extends Component {
  render () {
    return (
      <div>
        <section className='ftco-section ftco-bg-dark' id='process'>
          <div className='container'>
            <div className='row justify-content-center mb-5'>
              <div className='col-md-7 heading-section text-center ftco-animate'>
                <span className='subheading'>Services</span>
                <h2 className='mb-4'>Our Process</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi augue mi, lobortis accumsan mi. Sed vitae vestibulum purus.</p>
              </div>
            </div>
            <div className='row'>
              <div className='col-md-3 ftco-animate'>
                <div className='media d-block text-center block-6 services'>
                  <div className='icon d-flex justify-content-center align-items-center mb-5'>
                    <span className='flaticon-ruler'></span>
                  </div>
                  <div className='media-body'>
                    <h3 className='heading'>Analyze Your Goal</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi augue mi, lobortis accumsan mi. Sed vitae vestibulum purus.</p>
                  </div>
                </div>
              </div>
              <div className='col-md-3 ftco-animate'>
                <div className='media d-block text-center block-6 services'>
                  <div className='icon d-flex justify-content-center align-items-center mb-5'>
                    <span className='flaticon-gym'></span>
                  </div>
                  <div className='media-body'>
                    <h3 className='heading'>Work Hard On It</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi augue mi, lobortis accumsan mi. Sed vitae vestibulum purus.</p>
                  </div>
                </div>
              </div>
              <div className='col-md-3 ftco-animate'>
                <div className='media d-block text-center block-6 services'>
                  <div className='icon d-flex justify-content-center align-items-center mb-5'>
                    <span className='flaticon-tools-and-utensils'></span>
                  </div>
                  <div className='media-body'>
                    <h3 className='heading'>Improve</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi augue mi, lobortis accumsan mi. Sed vitae vestibulum purus.</p>
                  </div>
                </div>
              </div>
              <div className='col-md-3 ftco-animate'>
                <div className='media d-block text-center block-6 services'>
                  <div className='icon d-flex justify-content-center align-items-center mb-5'>
                    <span className='flaticon-abs'></span>
                  </div>
                  <div className='media-body'>
                    <h3 className='heading'>Achieve Perfect Body</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi augue mi, lobortis accumsan mi. Sed vitae vestibulum purus.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

Body.propsType = {

}

export default Body
