import { connect } from 'react-redux'

import Home from '../components/Home'

const mapActionCreators = {
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapActionCreators)(Home)
