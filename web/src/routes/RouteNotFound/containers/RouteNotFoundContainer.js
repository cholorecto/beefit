import { connect } from 'react-redux'

import RouteNotFound from '../components/RouteNotFound'

const mapActionCreators = {
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, mapActionCreators)(RouteNotFound)
