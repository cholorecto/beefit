import RouteNotFoundContainer from './containers/RouteNotFoundContainer'

export default (store) => ({
  path: '*',
  component: RouteNotFoundContainer
})
